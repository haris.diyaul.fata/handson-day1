import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CharacterizedString{
    public static void main(String[] args) throws IOException {
        String kalimat;

        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);

        System.out.print("Ketik kata/kalimat, lalu tekan enter: ");
        kalimat = br.readLine();

        System.out.println("=============================");
        System.out.println("String penuh: "+kalimat);
        System.out.println("=============================");

        System.out.println("Proses memecahkan kata/kalimat menjadi karakter............");
        System.out.println("Done.");
        System.out.println("=============================");
        for(int i = 0; i < kalimat.length(); i++){
            System.out.println("Karakter["+(i+1)+"]: "+kalimat.charAt(i));
        }
        System.out.println("=============================");
    }
}